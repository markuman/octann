classdef octann

    properties
      repeat_training
      Wh
      Wz
      output_activation
    end
    
    properties (Constant = true)
      % activation functions - also for hidden layers
      % requires always a derivative function too
      sigmoid = @(x) 1 ./ (1 + exp(1).^-x);
      dsigmoid = @(x) x .* (1 - x);
      
      linear = @(x) x;
      dlinear = @(x) 1;
      
      hyperbolic_tangent = @(x) tanh(x);
      dhyperbolic_tangent = @(x) 1 - tanh(x)^2;
      
      % activation functions only for output layer
      threshold = @(x) x > 0;
    end
    
    methods
      function self = octann(inputs, hidden_layers, outputs, varargin)
        p               = inputParser();
        p.CaseSensitive = true;
        p.FunctionName  = 'octann'; 
        % addParamValue is not recommended to use in matlab... however
        % addParameter is not implemented in octave yet, so we've to
        % use it
        p.addParamValue ('repeat_training', 1, @isnumeric);
        p.parse (varargin{:});
        self.repeat_training = p.Results.repeat_training;
        self.output_activation = self.sigmoid;

        % hidden layer weights
        self.Wh = rand(inputs, hidden_layers);

        % output layer weights
        self.Wz = rand(hidden_layers, outputs);
  
      end
    
      function self = train(self, data_input, desired_output, learning_rate)
        for n = 1:self.repeat_training

          % run the network
          [estimated_result, hidden_layer_result] = self.run(data_input);

          % calculate the Error
          E = desired_output - estimated_result;

          % delta estimated_result
          dZ = E * learning_rate;
          
          % delta hidden layer weights
          dH = (dZ * self.Wz') .* self.dsigmoid(hidden_layer_result);

          % update output layer weights
          self.Wz = self.Wz + (hidden_layer_result' * dZ);

          % update hidden layer weights
          self.Wh = self.Wh + (data_input' * dH);
        end
      end

      function [estimated_result, hidden_layer_results] = run(self, data_input)
        hidden_layer_results = self.sigmoid(data_input * self.Wh);
        estimated_result = self.output_activation(hidden_layer_results * self.Wz);
      end
		
    end % methods
end
