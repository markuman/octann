testClassdef = @(x) strcat('CLASSDEF FILE: ', x);

## LEARN XOR
data_input = [0, 0; 0, 1; 1, 0; 1, 1];
desired_output = [0; 1; 1; 0];
learning_rate = .1;
nn = octann(2, 3, 1, 'repeat_training', 10000);
nn = nn.train(data_input, desired_output, learning_rate);
retval = round(nn.run(data_input));
assert(retval(1) == 0, 
	testClassdef('learn xor must be 0'))

assert(retval(2) == 1, 
	testClassdef('learn xor must be 1'))
	
assert(retval(3) == 1, 
	testClassdef('learn xor must be 1'))
	
assert(retval(4) == 0, 
	testClassdef('learn xor must be 0'))


  
## LEARN AND

data_input = [0, 0; 0, 1; 1, 0; 1, 1];
desired_output = [0; 0; 0; 1];
learning_rate = .8;

nn = octann(2, 3, 1, 'repeat_training', 2000);
nn = nn.train(data_input, desired_output, learning_rate);
nn.output_activation = nn.threshold;

retval = round(nn.run(data_input));
assert(retval(1) == 0, 
	testClassdef('learn AND must be 0'))

assert(retval(2) == 0, 
	testClassdef('learn AND must be 0'))
	
assert(retval(3) == 0, 
	testClassdef('learn AND must be 0'))
	
assert(retval(4) == 1, 
	testClassdef('learn AND must be 1'))