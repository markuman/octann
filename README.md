# octann

Octann is a minimal and simple library for training and using feedforward artificial neural networks (ANN) in GNU Octave and Matlab.  

# usage

* __init a neural network__

```matlab
number_of_inputs = 2;
number_of_neurons_in_hidden_layer = 3;
number_of_outputs = 1;
nn = octann (number_of_inputs, number_of_neurons_in_hidden_layer, number_of_outputs);
nn.repeat_training
ans =  1
```

The default `repeat_training` is `1`. You can change the value in the object itself or change while init the network.

```
nn = octann (number_of_inputs, number_of_neurons_in_hidden_layer, number_of_outputs, 'repeat_training', 5000);
nn.repeat_training
ans =  5000
```

* __train the network__

```matalb 
input = [0, 0; 0, 1; 1, 0; 1, 1];
desired_output = [0; 1; 1; 0];
learning_rate = .1;
nn = nn.train(input, desired_output, learning_rate);

```

* __run the network__

```matlab
nn.run(input)
ans =

   0.0422092
   0.9819420
   0.9819409
   0.0049450

```


# unittesting

```
octave:1> mUnittest('test_octann')


 mUnittest: test_octann 


1 	 ✓ 	 CLASSDEF FILE:learn xor must be 0
2 	 ✓ 	 CLASSDEF FILE:learn xor must be 1
3 	 ✓ 	 CLASSDEF FILE:learn xor must be 1
4 	 ✓ 	 CLASSDEF FILE:learn xor must be 0
5 	 ✓ 	 CLASSDEF FILE:learn AND must be 0
6 	 ✓ 	 CLASSDEF FILE:learn AND must be 0
7 	 ✓ 	 CLASSDEF FILE:learn AND must be 0
8 	 ✓ 	 CLASSDEF FILE:learn AND must be 1

 PASSED 8 OF 8 


```

# status / limitation

* early development status
* only `sigmoid` is available as activation function
* supports only one hidden layer

# Inspiration

* [genann](https://github.com/codeplea/genann)
* [neuralpy](https://github.com/jon--lee/neuralpy)
* [luann](https://github.com/wixico/luann)
* [tinn](https://github.com/glouw/tinn)
